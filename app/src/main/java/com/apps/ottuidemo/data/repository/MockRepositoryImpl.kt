package com.apps.ottuidemo.data.repository

import com.apps.ottuidemo.data.remote.MockAPi
import com.apps.ottuidemo.data.remote.Resource
import com.apps.ottuidemo.data.remote.dto.MovieDto
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class MockRepositoryImpl @Inject constructor(
private val aPi: MockAPi
): MockRepository {

    override suspend fun getMovieList(pageNo:Int): Resource<MovieDto> {
        return try {
            Resource.Loading(null)
            val data=aPi.mockList(pageNo)
            Resource.Success(data)
        }  catch(e: HttpException) {
            Resource.Error<MovieDto>(e.localizedMessage ?: "An unexpected error occured")
        } catch(e: IOException) {
            Resource.Error<MovieDto>("Couldn't reach server. Check your internet connection.")
        }catch (e : Exception){
            Resource.Error<MovieDto>(e.localizedMessage ?: "An unexpected error occured")

        }


    }
}