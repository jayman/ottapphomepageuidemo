package com.apps.ottuidemo.data.remote.dto

import com.google.gson.annotations.SerializedName

data class MovieDto (
    @SerializedName("page" ) var page : Page? = Page()
)




