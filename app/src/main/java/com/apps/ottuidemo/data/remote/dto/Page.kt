package com.apps.ottuidemo.data.remote.dto

import com.apps.ottuidemo.R
import com.google.gson.annotations.SerializedName


data class Page (

    @SerializedName("title"               ) var title               : String?        = null,
    @SerializedName("total-content-items" ) var totalContentItems : String?        = null,
    @SerializedName("page-num"            ) var pageNum            : String?        = null,
    @SerializedName("page-size"           ) var pageSize           : String?        = null,
    @SerializedName("content-items"       ) var contentItems       : ContentItems? = ContentItems()

)
data class ContentItems (

    @SerializedName("content" ) var content : ArrayList<Content> = arrayListOf()

)
data class Content (

    @SerializedName("name"         ) var name         : String? = null,
    @SerializedName("poster-image" ) var posterImage : String? = null

)
data class ContentDto(var title :String , var imageData:Int)
fun Content.toContentDto() :ContentDto{
    var title :String = name ?:""
    var imageData:Int= when (posterImage) {
        "poster1.jpg" -> {
            R.drawable.poster1
        }

        "poster2.jpg" -> {
            R.drawable.poster2
        }

        "poster3.jpg" -> {
            R.drawable.poster3
        }

        "poster4.jpg" -> {
            R.drawable.poster4
        }

        "poster5.jpg" -> {
            R.drawable.poster5
        }

        "poster6.jpg" -> {
            R.drawable.poster6
        }

        "poster7.jpg" -> {
            R.drawable.poster7
        }

        "poster8.jpg" -> {
            R.drawable.poster8
        }
        "poster9.jpg" -> {
            R.drawable.poster9
        }
        else -> {
            R.drawable.placeholder_for_missing_posters
        }
    }

return ContentDto(title=title,imageData=imageData)
}