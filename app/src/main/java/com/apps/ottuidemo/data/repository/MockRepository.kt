package com.apps.ottuidemo.data.repository

import com.apps.ottuidemo.data.remote.Resource
import com.apps.ottuidemo.data.remote.dto.MovieDto

interface MockRepository {
    suspend fun getMovieList(pageNo:Int): Resource<MovieDto>
}