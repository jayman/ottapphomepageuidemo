package com.apps.ottuidemo.data.remote

import com.apps.ottuidemo.data.remote.dto.MovieDto
import retrofit2.http.GET
import retrofit2.http.Path

interface MockAPi {


    @GET("testApi/{pageNo}")
    suspend fun mockList(@Path("pageNo") pageNo:Int): MovieDto
}