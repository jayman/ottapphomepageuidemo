package com.apps.ottuidemo.ui.LandingPage

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.apps.ottuidemo.R
import com.apps.ottuidemo.data.remote.dto.Content
import com.apps.ottuidemo.data.remote.dto.toContentDto


class ListingPageAdapter(var context: Context)  :
    RecyclerView.Adapter<ListingPageAdapter.ViewHolder>(), Filterable {

    var dataList = mutableListOf<Content>()
    var filterDataList = mutableListOf<Content>()
    var charString: String=""

    internal fun setDataList(dataList: List<Content>) {
        this.dataList.addAll(dataList)
        this.filterDataList = this.dataList
        notifyDataSetChanged()
    }



    // Provide a direct reference to each of the views with data items
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView
        var imgMoviewThumb: ImageView

        init {
            title = itemView.findViewById(R.id.tv_film_name)
            imgMoviewThumb = itemView.findViewById(R.id.iv_film_poster)
        }

    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListingPageAdapter.ViewHolder {

        // Inflate the custom layout
        var view = LayoutInflater.from(parent.context).inflate(R.layout.raw_main_listing, parent, false)
        return ViewHolder(view)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: ListingPageAdapter.ViewHolder, position: Int) {

        // Get the data model based on position
        val data = filterDataList[position].toContentDto()
        val item=data.title
        val filterText=charString
        // Highlight the filtered text in the item
        var startPos = -1
        var endPos = -1

        val itemLowerCase = item.toLowerCase()
        val filterTextLowerCase = filterText.toLowerCase()

        // Find the starting position of the filtered text in the item
        val index = itemLowerCase.indexOf(filterTextLowerCase)
        if (index >= 0) {
            startPos = index
            endPos = index + filterText.length
        }

        // If the filter text is found, apply highlighting to the matched substring
        if (startPos != -1 && endPos != -1) {
            val spannable = SpannableString(item)
            spannable.setSpan(ForegroundColorSpan(Color.YELLOW),
                startPos, endPos,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            holder.title.text = spannable
        } else {
            holder.title.text = item
        }
        holder.imgMoviewThumb.setImageResource(data.imageData)


    }

    //  total count of items in the list
    override fun getItemCount() = filterDataList.size


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                charString = constraint?.toString() ?: ""
                val filterDataList: MutableList<Content> = if (charString.isEmpty()) dataList else {
                    val filteredList = dataList.filter {
                        it.name?.replace(" ", "", true)
                            ?.contains(charString.replace(" ", "", true), true) == true
                    }
                    filteredList.toMutableList()
                }
                return FilterResults().apply {
                    count= filterDataList.size
                    values = filterDataList
                }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {

                filterDataList = if (results?.values == null)
                    ArrayList()
                else
                    results.values as ArrayList<Content>
                notifyDataSetChanged()
            }
        }
    }

}