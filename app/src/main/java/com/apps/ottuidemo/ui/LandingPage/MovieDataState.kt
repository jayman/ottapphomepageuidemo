package com.apps.ottuidemo.ui.LandingPage

import com.apps.ottuidemo.data.remote.dto.MovieDto


sealed class MovieDataState {
    object Loading : MovieDataState()
    data class Success(val data: MovieDto) : MovieDataState()
    data class Error(val error: String="") : MovieDataState()

}

