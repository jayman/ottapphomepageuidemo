package com.apps.ottuidemo.ui.LandingPage

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apps.ottuidemo.data.remote.Resource
import com.apps.ottuidemo.data.remote.dto.MovieDto
import com.apps.ottuidemo.data.repository.MockRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class MovieDataViewModel @Inject constructor(
private val repository: MockRepository
):ViewModel() {
    var currentPage = 1
    var totalPageLoad=1
    var currentPageLoad=1

    var lastResponse: MovieDto? = null

    private val _uiEvent =  MutableLiveData<MovieDataState>()
    val uiEvent :LiveData<MovieDataState> = _uiEvent

    init {
    viewModelScope.launch {
        getMovieData()
    }

}
   suspend fun getMovieData() {
      val resultData=  repository.getMovieList(currentPage)
       when (resultData) {
           is Resource.Success -> {
               delay(1000)
               val tempPageLoad=resultData.data?.page?.totalContentItems ?: return
               Log.e("TAG", "tempPageLoad $tempPageLoad" )
               totalPageLoad= if (tempPageLoad!!.toInt() >= totalPageLoad) tempPageLoad!!.toInt() else totalPageLoad
               currentPageLoad += resultData.data?.page?.pageSize?.toInt()!!

               Log.e("TAG", "getMovieData: $totalPageLoad  $resultData" )
               if(lastResponse == null) {
                   lastResponse = resultData?.data
               } else {
                   val oldArticles = lastResponse?.page?.contentItems?.content
                   val newArticles = resultData.data?.page?.contentItems?.content!!.toList()
                   oldArticles?.addAll(newArticles)
               }
               pushMovieState(MovieDataState.Success(resultData?.data))
               currentPage++
           }
           is Resource.Error -> {
               pushMovieState(MovieDataState.Error(resultData.message?:"An unexpected error occurred"))
           }
           is Resource.Loading -> {
               pushMovieState(MovieDataState.Loading)
           }
       }


   }
    private fun pushMovieState(state:MovieDataState){
        _uiEvent.postValue(state)

    }
}