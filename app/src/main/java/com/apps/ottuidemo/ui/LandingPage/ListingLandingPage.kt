package com.apps.ottuidemo.ui.LandingPage

import android.content.res.Configuration
import android.os.Bundle
import android.widget.AbsListView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apps.ottuidemo.databinding.ActivityListingLandingPageBinding
import com.apps.ottuidemo.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ListingLandingPage : AppCompatActivity() {
    private val viewModel: MovieDataViewModel by viewModels()
    private lateinit var binding: ActivityListingLandingPageBinding
    private lateinit var listingAdapter: ListingPageAdapter

    private var isSearchActive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListingLandingPageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()
        setupObservers()
        setupListeners()
    }

    private fun setupRecyclerView() {
        val spanCount = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 3 else 7
        binding.rvMovieList.apply {
            this.layoutManager = GridLayoutManager(this@ListingLandingPage, spanCount, GridLayoutManager.VERTICAL, false)
            this.clipToPadding = false
            listingAdapter = ListingPageAdapter(applicationContext)
            this.adapter = listingAdapter
            this.addOnScrollListener(scrollListener)
        }
    }

    private fun setupObservers() {
        viewModel.uiEvent.observe(this, Observer { state ->
            when (state) {
                is MovieDataState.Success -> {
                    state.data.page?.contentItems?.content?.toList()?.let { it1 ->
                        listingAdapter.setDataList(it1)
                    }
                }
                is MovieDataState.Error -> {
                    Toast.makeText(this@ListingLandingPage, state.error, Toast.LENGTH_LONG).show()
                }
                MovieDataState.Loading -> {}
            }
        })
    }

    private fun setupListeners() {
        binding.headerLayout.ivSearch.setOnClickListener {
            if (listingAdapter.dataList.isNotEmpty()) {
                binding.headerLayout.edSearch.visible()
                binding.headerLayout.ivSearch.gone()
                binding.headerLayout.tvHeaderTitle.gone()
                binding.headerLayout.edSearch.requestFocus()
                showKeyboard()
            }
        }

        binding.headerLayout.edSearch.onDrawableEndClick {
            binding.headerLayout.edSearch.setText("")
            listingAdapter.filter.filter("")
            hideKeyboard()
        }

        binding.headerLayout.ivBack.setOnClickListener {
            if (binding.headerLayout.edSearch.isVisible) {
                binding.headerLayout.edSearch.clearFocus()
                binding.headerLayout.edSearch.gone()
                binding.headerLayout.ivSearch.visible()
                binding.headerLayout.tvHeaderTitle.visible()

                binding.headerLayout.edSearch.setText("")
                listingAdapter.filter.filter("")

                hideKeyboard(binding.headerLayout.edSearch)
            } else {
                finish()
            }
        }

        var job: Job? = null
        var previousCharCount = 0
        binding.headerLayout.edSearch.addTextChangedListener { editable ->
            isSearchActive = true
            job?.cancel()
            job = MainScope().launch {
                delay(Constant.SEARCH_NEWS_TIME_DELAY)
                editable?.let {
                    if (editable.toString().isNotEmpty() && editable.toString().length >= 3) {
                        listingAdapter.filter.filter(editable)
                    } else if (previousCharCount > editable.toString().length) {
                        listingAdapter.filter.filter("")
                    }
                    previousCharCount = editable.toString().length
                    isSearchActive = false
                }
            }
        }
    }

    private var isLoading = false
    private var isLastPage = false
    private var isScrolling = false

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = recyclerView.layoutManager as GridLayoutManager
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount

            val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
            val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount
            val isNotAtBeginning = firstVisibleItemPosition >= 0
            val isTotalMoreThanVisible = totalItemCount >= viewModel.totalPageLoad
            val shouldPaginate = isNotLoadingAndNotLastPage && isAtLastItem && isNotAtBeginning &&
                    !isTotalMoreThanVisible && isScrolling && !isSearchActive

            if (shouldPaginate) {
                lifecycleScope.launch {
                    viewModel.getMovieData()
                }
                isScrolling = false
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }
        }
    }

    override fun onBackPressed() {
        if (binding.headerLayout.edSearch.isVisible) {
            binding.headerLayout.edSearch.gone()
            binding.headerLayout.ivSearch.visible()
            binding.headerLayout.tvHeaderTitle.visible()
            hideKeyboard()
        } else {
          finish()
        }
    }
}
