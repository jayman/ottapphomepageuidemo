package com.apps.ottuidemo.di

import com.apps.ottuidemo.data.remote.MockAPi
import com.apps.ottuidemo.data.remote.MockInterceptor
import com.apps.ottuidemo.data.repository.MockRepositoryImpl
import com.apps.ottuidemo.data.repository.MockRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideMockApi(): MockAPi {
        return Retrofit.Builder()
            .client(OkHttpClient.Builder().addInterceptor(MockInterceptor()).build())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://www.google.com/")
            .build()
            .create(MockAPi::class.java)
    }

    @Provides
    @Singleton
    fun provideMockApiRepository(aPi: MockAPi): MockRepository {
        return MockRepositoryImpl(aPi)
    }

}