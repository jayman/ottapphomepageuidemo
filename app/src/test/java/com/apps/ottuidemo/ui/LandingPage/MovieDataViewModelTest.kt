package com.apps.ottuidemo.ui.LandingPage

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.apps.ottuidemo.data.remote.Resource
import com.apps.ottuidemo.data.remote.dto.MovieDto
import com.apps.ottuidemo.data.repository.MockRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class MovieDataViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: MockRepository

    @Mock
    private lateinit var uiEventObserver: Observer<MovieDataState>

    private lateinit var viewModel: MovieDataViewModel
    private lateinit var testDispatcher: TestCoroutineDispatcher
    private lateinit var testScope: TestCoroutineScope

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        testDispatcher = TestCoroutineDispatcher()
        testScope = TestCoroutineScope(testDispatcher)
        viewModel = MovieDataViewModel(repository)
        viewModel.uiEvent.observeForever(uiEventObserver)
    }

    @After
    fun cleanup() {
        viewModel.uiEvent.removeObserver(uiEventObserver)
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `getMovieData should update uiEvent with Success state when repository returns success`() =
        testScope.runBlockingTest {
            // Arrange
            val movieDto = MovieDto(/* movie data */)
            val successResource = Resource.Success(movieDto)
            Mockito.`when`(repository.getMovieList(1)).thenReturn(successResource)

            // Act
            viewModel.getMovieData()

            // Assert
            Mockito.verify(uiEventObserver).onChanged(MovieDataState.Success(movieDto))
        }

    @Test
    fun `getMovieData should update uiEvent with Error state when repository returns error`() =
        testScope.runBlockingTest {
            // Arrange
            val errorMessage = "An unexpected error occurred"
            val errorResource = Resource.Error(errorMessage,data = null)
            Mockito.`when`(repository.getMovieList(1)).thenReturn(Resource.Error(errorMessage,data = null))

            // Act
            viewModel.getMovieData()

            // Assert
            Mockito.verify(uiEventObserver).onChanged(MovieDataState.Error(errorMessage))
        }

    @Test
    fun `getMovieData should update uiEvent with Loading state when repository returns loading`() =
        testScope.runBlockingTest {
            // Arrange
            val loadingResource = Resource.Loading(null)

            Mockito.`when`(repository.getMovieList(1)).then {  }

            // Act
            viewModel.getMovieData()

            // Assert
            Mockito.verify(uiEventObserver).onChanged(MovieDataState.Loading)
        }
}